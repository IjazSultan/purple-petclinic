# Instructions on how to set up the mysql test databse

## prequisites 

A k8 cluster machine with docker installed on

an environmental variable set

## image info

```bash

# adding an 8.0.26
sudo docker pull mysql:8.0.26

# running the image, assign it a name, feed it a password mapped to env variable TEST_DB_PW and telling it what image to run the container from
sudo docker run --name mysql-test-db -e MYSQL_ROOT_PASSWORD=$TEST_DB_PW -d mysql:8.0.26
```
