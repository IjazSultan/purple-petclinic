# Kubernetes - Recovery

Welcome! This document will outline the steps taken to get a Kubernetes cluster environment up and running again **in the event of a disaster**. We want this to work so that we can take advantage of containers, and have resilience and high availability of this services with the use of a Load Balancer which has multi availability zones.

For the purpose of this assessment/document, let's assume that you have the following pre-requisites:

- k8 master node
- k8 worker node A
- k8 worker node B

All of which exist on AWS, and have Kubernetes installed and configured, and are able to communicate with each other via authorised host keys and private IPs.

You also have docker installed.

You have a docker image that exists in the registry.

## Steps for Assessment 4

The steps in this section were initially set up manually on the k8 master node. Once this is complete, the deployment will be successfully automated through a job on Jenkins named: 04 deploy petclinic to k8

On your k8 master node:

1. Enter the command to pull the docker image from the registry with `docker pull <name_of_image>`
2. Confirm the image is now on the machine locally with `sudo docker images`
3. Create a namespace e.g `purple-ns`
4. From our repo, copy the following files into the k8 master node:

- purple-deploy.yml (used to connect to the registry and external RDS and deploy)
- purple-service-cluster.yml (connects the deployment to the cluster via an IP)
- purple-ingress.yml (ingress rule to connect the Loadbalancer to DNS)
- externalrds.yml (exposes the private k8 cluster to the external RDS service)

Note: ensure that the purple-deploy.yml file has the correct image name as an argument, it must match the image returned in step 2:

```yml
image: <name_of_image>:$L_RDS
```

Note: ensure that the purple-ingress.yml file has the correct DNS name you desire, this is important for when you set up the DNS on AWS Route 53.

## LoadBalancer and DNS Configuration (Completed manually, exists on AWS)

1. Type in the command `kubectl get svc -n haproxy-controller`
2. For the haproxy-ingress service, make note of the port number after 80:, this will be used when setting up the Loadbalancer
3. Create a Target Group
   1. Select IP addresses on Basic Configuration
   2. Give the Target Group a name
   3. On Register Target page, in the IPv4 address field, enter your k8 master node's private IP address
   4. In the ports field, add the port number obtained from step 2
4. On AWS, create a new Application LoadBalancer, and only make the following changes:
   1. Give it a name
   2. On mapping, select all availability zones
   3. Give it cohort8-homeips and ALOffice SGs alongside the default VPC SG.
   4. Select the Target Group made in step 3.
   5. Select create Loadbalancer
5. Create a DNS on AWS Route 53
   1. CNAME
   2. Give it a DNS name
   3. Copy and paste the Loadbalancer DNS name into the value field

## CONGRATULATIONS

Now, your setup is complete.. again.

Now, you will run the Jenkins job 04 deploy petclinic to k8.