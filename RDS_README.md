# Mapping an RDS to your cluster and assigning new DNS

## Pre requisite:
- RDS database
- K8 Cluster


When you create an RDS database on AWS you are given an endpoint URI that follows the naming convention <database_name>.<1535362bunchofnumbers>.availabilityzone.rds.amazon.com
e.g 
`purple-rds-db2.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com`

## Exposing the external database

To expose the database to the cluster you need to create a service yaml file for the service `ExternalName` following this structure :
```yaml
spec:
  type: ExternalName
  externalName: <dns_of_service>
  ports:
  - name: "external-service"
    protocol: TCP
    port: 3306
    targetPort: 3306
    nodePort: 0
  selector: {}
```
#### Port 3306 is the port that allows MYSQL Databases to communicate with other instances

After creating this service apply using :

`kubectl apply -f <servicename.yml> -n <namespace> 