# Jenkins Setup

This document depicts the steps taken to set up the Jenkins infrastructure so that you will be able to delegate tasks to nodes and complete jobs.

TLDR: a summary of this document:

- Created a Jenkins Agent and Controller
- Added both of their public IP to the cohort8-homeips security group
- Confirmed Jenkins config page is up running
- Generated a key from controller and added this to BitBucket
- Configured Agent1 build to allow comms between agent and controller
- Added the necessary BitBucket keys
- Added the jenkins URL to the BitBucket Webhook
- The job can push to bitbucket and merge new code to master
- The job works with slack notifications

## Jenkins Agent Creation

Using the existing AMI from assessment 3 **purple-jenkins-agent**, I have launched a t3a.medium, ec2 instance and have named this `4-purple-jenkins-agent`, 34.244.191.65.

## Jenkins Controller Creation

Using the existing AMI from assessment 3 **purple-jenkins-controller**, I have launched a t2.micro, ubuntu instance and have named this `4-purple-jenkins-controller`, 52.214.3.188.

This already has dependencies such as jenkins, java and terraform installed from our previous project.

I then started and enabled the service using the following commands:

```bash
sudo systemctl start jenkins
sudo systemctl enable jenkins
sudo systemctl status jenkins # Confirming if active
```

The configuration page can now be accessed on:

http://52.214.3.188:8080/

NOTE: Remember to add the relevant Bitbucket IP security group to the servers so they are able to communicate with Bitbucket.

## BitBucket Config - Connecting Jenkins to BitBucket

Generated a key on the controller using `ssh-keygen` and added its public credentials on BitBucket.

Repository settings -> Access keys -> Add key -> Paste public key

## Jenkins Configuration

This section outlines the changes I made on the Jenkins site.

### Agent Setup

Since this was launched from an AMI from last assessment, the worker node has already mostly been configured on the Jenkins page.

Agent1 -> Confirguation settings:

I added the private IP of the newly created 4-jenkins-agent instance into the configuration page to allow communication between controller and agent.

On the controller node, I used the following commands to generate a new key:

```bash
sudo su - jenkins
ssh-keygen -f ~/.ssh/4_jenkins_agent_key
```

Then gave this private key to Jenkins Agent1.

### Job

General:

- Changed project URL (new BitBucket repo link for assessment 4)
- Restricted where this project can be run (Agent1 only)

Source Code Management:

- Repository URL: git@bitbucket.org:IjazSultan/purple-petclinic.git
- Gave it Steve's fake key because Jenkins has some issues identifying stuff, sometimes.

**NOTE: might want to add an additional behaviour to check out to a subdirectory.**

### Creating a new job

- Source code management (same as above)
- Added URL to bitbucket repository
- Restricted job run to Agent1
- Added a branch `dev-*`

### Post to Slack

Steps:

- Install Slack Notification plugin on Jenkins
- Created a channel named `slack-notifier-purple`
- On Slack app directory, I added the channel to the Jenkins app, which returned a secret token
- Added this token to Jenkins configuration as a secret text so Jenkins now has authorisation to post to the specified channel


```bash

```

## Worker Node Setup (k8-master-node)

You need Java installed. The script below was used:

```bash
sudo yum update -y
sudo yum install software-properties-common apt-transport-https -y
sudo yum install java-1.8.0-openjdk -y
```

Now, we must add the following commands on the agent node:

```bash
sudo useradd -m -s /bin/bash jenkins
sudo passwd jenkins
```

This creates a new user on the master k8 node (agent node)

You also need to give the public key that was generated in the controller, to the agent's authorised keys.

Then you need to manage the node and add jobs.
