# Jenkins documentation

*Jenkins log in credentially will be passed on to management securely and will not be found in this repo*
the Jenkins is set up with 3 jobs currently:
*All jobs can be triggered manually but also have their own automatic trigger, described below*

- test-job-petclinic
- update-docker-registry
- k8-latest-image-deploy

## 01 petclinic tests

the steps of this job are as follows:

*for manual trigger of this job please ensure the correct code is uncommented in the execute shell within jenkins*

- triggers when a change is made to any dev branch
- pulls the dev branch repo 
- runs tests on the dev branch repo
- if tests pass then it merges the dev branch with the master branch

## 02 update-docker-registry

the steps of this job are as follows:
**this is for local deployment (connected to petclinic internal database (h2))**

- triggers when `01 petclinic tests` is successful
- starts docker incase it is inactive
- pulls the master repo
- takes the petclinic/Dockerfile in the repo and builds a new image of it
- pushes the image to the docker-registry, applying the jenkins environment variable 
`BUILD_ID` to the image tag
- creates a file `/home/ec2-user/docker-reg-version` that stores the value of `BUILD_ID` to a new environmental variable `L` and gives it permissions
- the file is then sourced and the scp command is used to copy it to the k8 machine

**Note: The jenkins agent (Agent1) must have the purplessh.pem file, containing the private key, with chmod 400 priviledges set. The k8 (Agent2) machine needs to allow the jenkins agent access via inbound security rules**

## 03 update-docker-registry-rds

the steps of this jobs are as follows:
**this is for production environment (connected to the rds)**

- triggers when `01 petclinic tests` is successful
- starts docker incase it is inactive
- pulls the master repo
- takes the petclinic/Dockerfile in the repo and builds a new image of it
- pushes the image to the docker-registry, applying the jenkins environment variable 
`BUILD_ID` to the image tag
- creates a file `/home/ec2-user/docker-reg-version-rds` that stores the value of `BUILD_ID` to a new environmental variable `L_RDS` and gives it permissions
- the file is then sourced and the scp command is used to copy it to the k8 machine

**Note: The jenkins agent (Agent1) must have the purplessh.pem file, containing the private key, with chmod 400 priviledges set. The k8 (Agent2) machine needs to allow the jenkins agent access via inbound security rules**

## Project 04 deploy petclinic to k8

- triggers when `03 update-docker-registry` is successful
- starts docker incase it is inactive
- sources the copied over file containing the `BUILD_ID` stored as the environmental variable `L_RDS`
- Applies the `purple-deploy.yml` file, which references the latest petclinic image using `$L_RDS`
- if the machine needs to be built from fresh the last 2 lines in the jenkins job can be uncommented, this will reapply the `purple-ingress.yml` and `purple-service-cluster.yml` files.

These jobs are also visualised in the below flow diagram.
![flow](jenkins-flowdiagram.png)
