# objective: private docker registry setup

prerequisites: 

- jenkins agent node (EC2) with docker installed (amazon linux machine)
- Docker-registry (EC2) with docker and docker-compose installed (ubuntu 18.04)

*Note: using an amazon linux for the docker-registry machine ran into problem with the ui image so current requires an ubuntu machine*

docker install commands to run on jenkins agent node:

```bash

# Update the installed packages and package cache on your instance.
sudo yum update -y

# Install the most recent Docker Engine package.
sudo amazon-linux-extras install docker

sudo yum install docker
Start the Docker service.

sudo service docker start
```

docker install commands for docker-registry instance (including docker-compose install):

ubuntu docker install link: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04

```bash

# First, update your existing list of packages:
sudo apt update
 
# Next, install a few prerequisite packages which let apt use packages over HTTPS:
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
 
# Then add the GPG key for the official Docker repository to your system:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
 
#Add the Docker repository to APT sources:
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
 
# Next, update the package database with the Docker packages from the newly added repo:
sudo apt update
 
# Make sure you are about to install from the Docker repo instead of the default Ubuntu repo:
apt-cache policy docker-ce

# Finally, install Docker:
sudo apt install docker-ce -y
 
# Docker should now be installed, the daemon started, and the process enabled to start on boot. Check that it’s running:
sudo systemctl status docker

# install docker compose
sudo apt install docker-compose -y
```

On the docker-registry machine following the below commands 

```bash

mkdir docker-registry

cd docker-registry

mkdir volume

touch docker-compose.yaml

```

Add the contents of docker-compose.yaml to the docker-registry machine's docker-compose.yaml file.

once this is done run the command `sudo docker-compose -f docker-compose.yaml up` you can add `-d` to make sure this is run in background.

To compose down `sudo docker-compose down`

once successful the docker-registry ui should appear at 34.247.32.254:8080 
IP COULD CHANGE IF MACHINE IS RESTARTED (docker-registry-public-ip:8080)

**The below commands are executed on the jenkins agent node**

*NOTE editting the /etc/hosts file and /etc/docker/daemon.json file needs to be done on any machine that needs access to the docker-registry as well as giving them correct security access as mentioned below via security groups*

```bash

# open the hosts file.
sudo nano /etc/hosts

# add in the following: the public ip of the docker-registry machine, and map it to a value.
34.247.32.254 docker.local.com
# NOTE IF DOCKER-REGISTRY MACHINE IS SWITCHED OFF THIS PUBLIC IP MAY CHANGE. 
# AND NEED UPDATING IN THIS FILE.
# save the file.

# now attempt to ping the public ip
ping docker.local.com
# if this works a connection has been established, note security groups 
# allowing inbound traffic from the agent node to the docker-registry machine 
# is required and also a security group allowing inbound traffic from the 
# docker-registry machine to the agent node. these are created and added on
# the aws console.

# 
sudo nano /etc/docker/daemon.json

#add the below information to the daemon.json file
{ 
   "insecure-registries": ["docker.local.com:5000"]
}
# this is to authentication communication between the agent and docker-registry machine

# to ensure the service uses the updated daemon.json restart the system
sudo systemctl restart docker

# check docker is active again
sudo systemctl status docker

# if for whatever reason restart does not work try stop and start commands. 
# You can run the above command between stoo and start commands to check they 
# are working correctly
sudo systemctl stop docker

sudo systemctl start docker
# check the status after again.

# now the agent can ping the machine and authentication has been 
# established, it is time to test pulling and pushing images

# to check what images are currently accessible to the agent node
sudo docker images

# testing pulling an alpine image from the docker hub
sudo docker pull alpine

# this should be visible in docker images check with the below
sudo docker images

# this tags the image with docker-registry host name you start by specifying 
# the current image and version (alpine:latest) then provide the host name 
# (docker.local.com:5000) followed by which user that is creating the image 
# (jenkins-agent)and then image name and version(alpine-jenkins-agent:v1)
sudo docker tag alpine:latest docker.local.com:5000/jenkins-agent/alpine-jenkins-agent:v1

sudo docker images
# this should show the newly tagged image

# this will push the new image to the registry
sudo docker push docker.local.com:5000/jenkins-agent/alpine-jenkins-agent:v1
# this should now be visible in the ui interface


# removing an image from the docker machine
sudo docker rmi docker.local.com:5000/jenkins-agent/alpine-jenkins-agent:v1

# to pull an image from the registry naviagte the ui at 
# http://18.203.47.176:8080/ until you select the desired repo and then 
# desired image in the case of the above the full link is 
# http://18.203.47.176:8080/tag/jenkins-agent/alpine-jenkins-agent/v1/ 
# once here copy the pull request command 
# "docker pull docker-registry:5000/jenkins-agent/alpine-jenkins-agent:v1" 
# edit the "docker-registry" part and add sudo to the front 
# in our jenkins agent machine this command would appear as the below
sudo docker pull docker.local.com:5000/jenkins-agent/alpine-jenkins-agent:v1
```

Currently deleting individual images from private registries is not supported via the docker API docs. This has not been covered in this documentation and would need further research and implementation to make possible. 