# Building Petclinic Images

Step to build a Petclinic Docker image:

1. Pull master code into a dev-* branch
2. Do the changes to the Petclinic files within the "petclinic" folder you want
3. Commit and push your code to bitbucket
4. Jenkins will use a webhook to automatically pull the new code
5. Jenkins tests new code from the dev-* branch
6. Jenkins pushes new code to master (if tests are succesful)
7. Jenkins builds new Docker image of Petclinic
8. Jenkins pushes new image to registry

At this point a new image has been created.

However to run the image on the production environment:

1. Manual or Automatic Jenkins job takes new code and builds a new Docker image but changes the "application.properties" and "application-mysql.properties" to allow RDS connection.
   
```
# Build image
sudo docker build . -t docker.local.com:5000/jenkins-agent/petclinic:v1

# Push image to registry
sudo docker push docker.local.com:5000/jenkins-agent/petclinic:v1
```
