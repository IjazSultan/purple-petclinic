# User guide for developers

to work on the repository follow these steps:
*Access to jenkins has been pased on to management*

1. clone the repo to your machine `git clone git@bitbucket.org:IjazSultan/purple-petclinic.git`
   
2. checkout a `dev-` branch
   
3. once changes are made push from your `dev-` branch
   
4. wait for tests to run on jenkins, you will be alerted via slack notification (slack channel: slack-notifier-purple) if this was successful or not. If you have access to jenkins this will also be viewable via the job `01 petclinic tests`. If successful, 2 further jobs `03 update docker registry rds` and `04 deploy petclinic to k8` will be ran pushing the new Petclinic image to the production environment. This can be seen at http://4petclinic.academy.labs.automationlogic.com/
   
5. if the `test-job-petclinic` job fails this means the changes were not safe to be deployed to production and will not be pushed to master or trigger subsequent jobs.

*Further details on the jenkins jobs can be found in the Jenkins directory on this repo*

## Local Petclinic Image

To test a build of Petclinic within your local machine you can pull an image from the registry.

The registry is viewable from [http://34.247.32.254:8080/home](http://34.247.32.254:8080/home).

Here you can view the images of petclinic that have been built through Jenkins.

`docker pull docker.local.com:5000/jenkins-agent/petclinic:<tag>`

Note that you will need to choose a version (tag) to pull.

You can then build a container from this image which will use a db within the container using:

`docker run -d -p 80:8080 docker.local.com:5000/jenkins-agent/petclinic:<tag>`


